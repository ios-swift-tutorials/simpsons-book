//
//  detailsViewController.swift
//  SimpsonBook
//
//  Created by Rumeysa Bulut on 4.12.2019.
//  Copyright © 2019 Rumeysa Bulut. All rights reserved.
//

import UIKit

class detailsViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var jobLabel: UILabel!
    
    var selectedSimpson : Simpsons?  // when it's not optional, then it requires an initializer for detailsViewController class.
                                    // Besides, constructor and its arguments have to be defined when it's not optional
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = selectedSimpson?.name
        jobLabel.text = selectedSimpson?.job
        imageView.image = selectedSimpson?.image
    }
    


}
