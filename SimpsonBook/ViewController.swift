//
//  ViewController.swift
//  SimpsonBook
//
//  Created by Rumeysa Bulut on 4.12.2019.
//  Copyright © 2019 Rumeysa Bulut. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var selectedCharacter : Simpsons?
    var simpsonFam = [Simpsons]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self // self is ViewController, tableview gets data from ViewController
        
        let homer = Simpsons(nameInit: "Homer Simpson", jobInit: "Nuclear Safety", imageInit: UIImage(named: "homer")!)
        let marge = Simpsons(nameInit: "Marge Simpson", jobInit: "House Wife", imageInit: UIImage(named: "marge")!)
        let lisa = Simpsons(nameInit: "Lisa Simpson", jobInit: "Student", imageInit: UIImage(named: "lisa")!)
        let bart = Simpsons(nameInit: "Bart Simpson", jobInit: "Student", imageInit: UIImage(named: "bart")!)
        let maggie = Simpsons(nameInit: "Maggie Simpson", jobInit: "Baby", imageInit: UIImage(named: "maggie")!)
        
        simpsonFam.append(homer)
        simpsonFam.append(marge)
        simpsonFam.append(lisa)
        simpsonFam.append(bart)
        simpsonFam.append(maggie)
        
        navigationItem.title = "Simpsons"
    }

    // To be able to use UITableViewDelegate and UITableViewDataSource protocols, we should add 2 functions below
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = simpsonFam[indexPath.row].name
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return simpsonFam.count
    }
    
    // Which row we selected
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCharacter = simpsonFam[indexPath.row]
        performSegue(withIdentifier: "toDetailVC", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetailVC" {
            let destinationVC = segue.destination as! detailsViewController
            destinationVC.selectedSimpson = selectedCharacter
        }
    }
    
}

