//
//  Simpsons.swift
//  SimpsonBook
//
//  Created by Rumeysa Bulut on 5.12.2019.
//  Copyright © 2019 Rumeysa Bulut. All rights reserved.
//

import Foundation
import UIKit

class Simpsons {
    var name : String = ""
    var job : String = ""
    var image : UIImage
    
    init(nameInit: String, jobInit: String, imageInit: UIImage) {
        name = nameInit
        job = jobInit
        image = imageInit
    }
    
    
}
